Ссылка на макет тестового задания:
https://www.figma.com/file/ReKYI0HETqzpQIdUu3fl3b/%D0%A2%D0%B5%D1%81%D1%82%D0%BE%D0%B2%D0%BE%D0%B5-frontend?node-id=0%3A1&viewport=543%2C901%2C0.21355457603931427


По данному макету необходимо сверстать страницу и реализовать имеющийся интерактив. Желательно интерактив реализовать на vue.js, чтобы показать свои знания этого фреймворка.
Ховеры в макете не указаны, можете сделать на свое усмотрение.

В интерактив входит:
- переключение табов в разделе "Наши услуги";
- Во втором разделе реализовать закрашивание пунктирной линии оранжевым цветом при скролле страницы и плавное появление трех карточек;
- слайдер в разделе "Результат нашей работы";
- В разделе "Нам доверяют" плавное смещение блока с парнерами влево при скролле вниз и вправо при скролле вверх;

